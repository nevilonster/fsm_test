import { FSMError, FSM, FSM_ERRORS, IFSM } from "libs/fsm";
import { alphabet, finiteState, startState, states, transitions } from "./fsmModThreeMock";

describe("FSM Test", () => {
  describe("Mod-Three FSM test", () => {
    const fsm: IFSM = new FSM(alphabet, states, startState, finiteState, transitions);
    let fsmError: FSMError;
    afterAll(() => {
      fsm.destroy();
    });
    beforeEach(() => {
      fsmError = null;
    });
    it("Input values `1010` should be 1", async () => {
      expect(fsm.exec("1010")).toEqual("1");
    });
    it("Input values `110` should be 0", async () => {
      expect(fsm.exec("110")).toEqual("0");
    });
    it("Input values `1102` should generate error UNSUPPORTED_SYMBOL_ERROR", async () => {
      try {
        fsm.exec("1102");
      } catch (e) {
        fsmError = e;
      } finally {
        expect(fsmError.code).toBe(FSM_ERRORS.UNSUPPORTED_SYMBOL_ERROR);
      }
    });
    it("Input values `1011` should generate error UNSUPPORTED_FINITE_STATE_ERROR", async () => {
      try {
        fsm.exec("1011");
      } catch (e) {
        fsmError = e;
      } finally {
        expect(fsmError.code).toBe(FSM_ERRORS.UNSUPPORTED_FINITE_STATE_ERROR);
      }
    });
  });
});
