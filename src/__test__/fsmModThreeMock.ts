import { Alphabet, FiniteState, State, TransitionFunction } from "libs/fsm";

export const alphabet: Alphabet = "01";
export const states: Array<State> = ["S0", "S1"];
export const startState: State = "S0";
export const transitions: TransitionFunction = new Map([
  [
    "S0",
    new Map([
      ["0", "S0"],
      ["1", "S1"],
    ]),
  ],
  [
    "S1",
    new Map([
      ["0", "S2"],
      ["1", "S0"],
    ]),
  ],
  [
    "S2",
    new Map([
      ["0", "S1"],
      ["1", "S2"],
    ]),
  ],
]);
export const finiteState: FiniteState = new Map([
  ["S0", "0"],
  ["S1", "1"],
]);
