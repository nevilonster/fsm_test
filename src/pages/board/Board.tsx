import { useState } from "react";
import styled from "styled-components";
import { StyledButton } from "globalStyles/theme/buttons";
import { ContentWrap, TitlePageWrap } from "globalStyles/pages";
import { FSMReactive, IFSM } from "libs/fsm";
import FSMComponent from "components/fsm/FSMComponent";
import {
  mockAlphabet,
  mockFiniteState,
  mockStartState,
  mockStates,
  mockTransitions,
} from "mock/mockSFM";

const Board = () => {
  const [fsm, setFSM] = useState<IFSM>(null);
  const onClick = () => {
    fsm?.destroy();
    setFSM(
      new FSMReactive(mockAlphabet, mockStates, mockStartState, mockFiniteState, mockTransitions)
    );
  };

  return (
    <ContentWrap>
      <TitlePageWrap>Board</TitlePageWrap>
      <ButtonContainerWrap>
        <StyledButton onClick={onClick}>Demo</StyledButton>
      </ButtonContainerWrap>
      {fsm && <FSMComponent fsm={fsm} />}
    </ContentWrap>
  );
};

export default Board;

const ButtonContainerWrap = styled.div`
  width: fit-content;
  display: flex;
  flex-direction: column;
  align-self: center;
  gap: 1rem;
`;
