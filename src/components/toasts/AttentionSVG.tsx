import React from "react";
type ComponentProps = {
  color: string;
};

const AttentionSVG = ({ color }: ComponentProps) => {
  return (
    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0_800_1643)">
        <circle cx="8" cy="8" r="7" stroke={color} strokeWidth="1.2" />
        <path
          d="M7.50649 9.42482L7 4.5H9L8.49351 9.42482H7.50649ZM7.09091 11.5V10.1199H8.90909V11.5H7.09091Z"
          fill="#DE0E10"
        />
      </g>
      <defs>
        <clipPath id="clip0_800_1643">
          <rect width="16" height="16" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default AttentionSVG;
