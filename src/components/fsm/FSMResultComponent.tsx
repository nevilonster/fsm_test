import { StyledButton } from "globalStyles/theme/buttons";
import Input, { Label } from "components/input/Input";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import FSMErrorToast from "components/toasts/FSMErrorToast";
import styled from "styled-components";
import { FiniteStateResult, FSMError } from "libs/fsm";
import FSMReactive from "libs/fsm/FSMReactive";
import { IFSM } from "libs/fsm/types";

type ComponentProps = {
  fsm: IFSM;
};
const FSMResultComponent = ({ fsm }: ComponentProps) => {
  const [inputValue, setInputValue] = useState("1101");
  const [err, setErr] = useState<FSMError>(null);
  const [result, setResult] = useState<FiniteStateResult>(null);

  useEffect(() => {
    const subscribe = (fsm as FSMReactive).result.subscribe(value => {
      setResult(value);
    });
    return () => {
      subscribe.unsubscribe();
    };
  }, [fsm]);

  useEffect(() => {
    err && toast.error(<FSMErrorToast description={err?.message} />);
  }, [err]);

  const onApply = () => {
    try {
      fsm.exec(inputValue);
    } catch (e) {
      setErr(e as FSMError);
    }
  };

  return (
    <Wrap>
      <Input
        placeholder={"Input"}
        onChange={e => setInputValue(e.currentTarget.value)}
        value={inputValue}
      />
      <StyledButton onClick={onApply}>Apply</StyledButton>
      {result && <Label>Result: {result}</Label>}
    </Wrap>
  );
};

export default FSMResultComponent;

const Wrap = styled.div`
  display: flex;
  flex-direction: row;
  gap: 1rem;
`;
