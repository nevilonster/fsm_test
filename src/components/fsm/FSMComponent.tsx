import styled from "styled-components";
import FSMResultComponent from "components/fsm/FSMResultComponent";
import FSMViewComponent from "components/fsm/FSMViewComponent";
import { IFSM } from "libs/fsm/types";

type ComponentProps = {
  fsm: IFSM;
};
const FSMComponent = ({ fsm }: ComponentProps) => {
  return (
    <Wrap>
      <FSMViewComponent fsm={fsm} />
      <FSMResultComponent fsm={fsm} />
    </Wrap>
  );
};

export default FSMComponent;

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1rem;
`;
