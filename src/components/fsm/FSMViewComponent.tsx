import styled from "styled-components";
import { TitlePageWrap } from "globalStyles/pages";
import { typography } from "globalStyles/theme/fonts";
import { IFSM } from "libs/fsm/types";

type ComponentProps = {
  fsm: IFSM;
};
const FSMViewComponent = ({ fsm }: ComponentProps) => {
  const generateViewByParam = (label, value) => {
    return (
      <ParamWrap key={"view_" + label}>
        <ParamLabel>{label}</ParamLabel>
        <ParamValue>{value}</ParamValue>
      </ParamWrap>
    );
  };

  return (
    <Wrap>
      <TitlePageWrap>Current FSM</TitlePageWrap>
      {generateViewByParam("Alphabet:", fsm.alphabet)}
      {generateViewByParam("States:", fsm.states)}
      {generateViewByParam("Initial State:", fsm.initialState)}
      {generateViewByParam("Final states:", fsm.finiteStatesToString)}
      {generateViewByParam("Transitions", fsm.transitionsToString)}
    </Wrap>
  );
};

export default FSMViewComponent;

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1rem;
  border: 1px solid ${({ theme }) => theme.colors.border.primary};
  border-radius: 4px;
  padding: 1rem;
`;

const ParamWrap = styled.div`
  display: flex;
  flex-direction: row;
  gap: 0.1rem;
  ${typography.smallText}
`;

const ParamLabel = styled.div`
  min-width: 200px;
`;
const ParamValue = styled.div``;
