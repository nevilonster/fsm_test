import React from "react";
import styled from "styled-components";
import { typography } from "globalStyles/theme/fonts";

const Header = () => {
  return (
    <Container>
      <TitleDetailWrap>Finite State Machine</TitleDetailWrap>
    </Container>
  );
};

export default Header;

const TitleDetailWrap = styled.div`
  ${typography.title}
  color: ${({ theme }) => theme.colors.bg.primaryTextColor};
  align-self: center;
`;

const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  box-sizing: border-box;
  padding: 1rem;
  height: 100%;
  background: ${({ theme }) => theme.colors.bg.primary};
  box-shadow: 0 2px 4px rgba(36, 36, 36, 0.16);
`;
