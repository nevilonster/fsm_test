import { Alphabet, FiniteState, State, TransitionFunction } from "libs/fsm";

export const mockAlphabet: Alphabet = "01";
export const mockStates: Array<State> = ["S0", "S1"];
export const mockStartState: State = "S0";
export const mockTransitions: TransitionFunction = new Map([
  [
    "S0",
    new Map([
      ["0", "S0"],
      ["1", "S1"],
    ]),
  ],
  [
    "S1",
    new Map([
      ["0", "S2"],
      ["1", "S0"],
    ]),
  ],
  [
    "S2",
    new Map([
      ["0", "S1"],
      ["1", "S2"],
    ]),
  ],
]);
export const mockFiniteState: FiniteState = new Map([
  ["S0", "0"],
  ["S1", "1"],
]);
