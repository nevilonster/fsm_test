import { ReactElement, ReactNode } from "react";
import { ROUTES } from "./enums";
import Board from "pages/board/Board";
import Layout from "components/layout/Layout";
import NotFoundPage from "pages/notFound/NotFoundPage";

export interface IRoute {
  path: string;
  fallback: NonNullable<ReactNode> | null;
  element: ReactElement;
  index?: boolean;
  layoutElement?: ReactElement;
}

export const routes: IRoute[] = [
  {
    path: ROUTES.DASHBOARD,
    element: <Board />,
    fallback: null,
    layoutElement: <Layout />,
  },
  {
    path: ROUTES.NOT_FOUND,
    element: <NotFoundPage />,
    fallback: null,
    layoutElement: <Layout />,
  },
];

export default routes;
