import { Alphabet, FiniteState, FiniteStateResult, IFSM, State, TransitionFunction } from "./types";
import FSMError, {
  UNSUPPORTED_FINITE_STATE_ERROR,
  NO_TRANSITION_ERROR,
  UNSUPPORTED_SYMBOL_ERROR,
} from "./FSMError";

class FSM implements IFSM {
  protected _alphabet: Alphabet;
  protected _states: Array<State>;
  protected _initialState: State;
  protected _transitions: TransitionFunction;
  protected _finiteStates: FiniteState;
  protected _currentState: State;

  /**
   * Finite State Machine
   * @param alphabet: Alphabet
   * @param states: Array<State>
   * @param initialState: State
   * @param finiteStates: FiniteState
   * @param transitions: TransitionFunction
   */
  constructor(
    alphabet: Alphabet,
    states: Array<State>,
    initialState: State,
    finiteStates: FiniteState,
    transitions: TransitionFunction
  ) {
    this._alphabet = alphabet;
    this._states = states;
    this._initialState = initialState;
    this._transitions = transitions;
    this._finiteStates = finiteStates;
    this._currentState = null;
  }

  private checkIsBelongAlphabet(symbol: string): boolean {
    const isInclude = this._alphabet.includes(symbol);
    if (!isInclude)
      throw new FSMError(
        UNSUPPORTED_SYMBOL_ERROR,
        `Symbol '${symbol}' is not supported on this FSM`
      );
    return isInclude;
  }

  private changeState(symbol: State): boolean {
    if (
      this._transitions.get(this._currentState) &&
      this._transitions.get(this._currentState).get(symbol)
    ) {
      this._currentState = this._transitions.get(this._currentState).get(symbol);
    } else {
      throw new FSMError(
        NO_TRANSITION_ERROR,
        `No transition from ${this._currentState} by ${symbol}`
      );
    }
    return true;
  }

  /**
   * @param value: string
   */
  public exec(value: string): FiniteStateResult {
    this.clean();

    for (const symbol of value) {
      this.checkIsBelongAlphabet(symbol);
      this.changeState(symbol);
    }
    const res = this._finiteStates.get(this._currentState);
    if (res === undefined) {
      throw new FSMError(
        UNSUPPORTED_FINITE_STATE_ERROR,
        `The ${this._currentState} state is missing in the final states`
      );
    }
    return res;
  }

  protected clean(): void {
    this._currentState = this._initialState;
  }

  get finiteStatesToString(): string {
    return JSON.stringify(Object.fromEntries(this._finiteStates));
  }
  get transitionsToString(): string {
    const obj = Object.fromEntries(this._transitions);

    return Object.entries(obj)
      .map(([key, value]) => {
        return key + JSON.stringify(Object.fromEntries(value));
      })
      .toString();
  }
  get initialState(): State {
    return this._initialState;
  }
  get states(): string {
    return this._states.toString();
  }
  get alphabet(): Alphabet {
    return this._alphabet;
  }

  public destroy(): void {
    console.warn("Destroy FSM");
  }
}

export default FSM;
