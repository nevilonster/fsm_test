export const UNSUPPORTED_SYMBOL_ERROR = "UNSUPPORTED_SYMBOL_ERROR";
export const NO_TRANSITION_ERROR = "NO_TRANSITION_ERROR";
export const UNSUPPORTED_FINITE_STATE_ERROR = "UNSUPPORTED_FINITE_STATE_ERROR";

class FSMError extends Error {
  readonly code: string;
  constructor(code, message) {
    super(message);
    this.code = code;
  }
}

export default FSMError;
