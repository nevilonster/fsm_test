export type State = string;
export type Alphabet = string;
export type TransitionFunction = Map<State, Map<Alphabet, State>>;
export type FiniteStateResult = string;
export type FiniteState = Map<State, FiniteStateResult>;

export interface IFSM {
  get alphabet(): Alphabet;
  get states(): string;
  get initialState(): State;
  get transitionsToString(): string;
  get finiteStatesToString(): string;

  exec(value: string): FiniteStateResult;
  destroy(): void;
}
