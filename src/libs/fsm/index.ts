import FSM from "./FSM";
import FSMReactive from "./FSMReactive";
import FSMError, {
  UNSUPPORTED_FINITE_STATE_ERROR,
  NO_TRANSITION_ERROR,
  UNSUPPORTED_SYMBOL_ERROR,
} from "./FSMError";
import { IFSM, Alphabet, FiniteState, FiniteStateResult, State, TransitionFunction } from "./types";

export { FSM, FSMReactive, FSMError };
export const FSM_ERRORS = {
  UNSUPPORTED_FINITE_STATE_ERROR,
  NO_TRANSITION_ERROR,
  UNSUPPORTED_SYMBOL_ERROR,
};
export type { IFSM, Alphabet, FiniteState, FiniteStateResult, State, TransitionFunction };
