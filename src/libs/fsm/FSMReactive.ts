import { BehaviorSubject } from "rxjs";
import { Alphabet, FiniteState, FiniteStateResult, State, TransitionFunction } from "./types";
import FSM from "./FSM";

class FSMReactive extends FSM {
  // used for subscribe to result changes
  private _result: BehaviorSubject<FiniteStateResult>;
  constructor(
    alphabet: Alphabet,
    states: Array<State>,
    initialState: State,
    finiteStates: FiniteState,
    transitions: TransitionFunction
  ) {
    super(alphabet, states, initialState, finiteStates, transitions);
    this._result = new BehaviorSubject<FiniteStateResult>(null);
  }

  override exec(value: string): FiniteStateResult {
    const res = super.exec(value);
    this._result.next(res);
    return res;
  }

  override clean(): void {
    super.clean();
    this._result.next(null);
  }

  public get result(): BehaviorSubject<FiniteStateResult> {
    return this._result;
  }

  override destroy() {
    super.destroy();
    this._result.next(null);
    this._result.complete();
  }
}

export default FSMReactive;
