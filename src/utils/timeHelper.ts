const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export const convertSecondsToTimeString = value => {
  const hours = Math.floor(value / 3600);
  const minutes = Math.floor((value % 3600) / 60);

  return hours + ":" + minutes.toString().padStart(2, "0");
};

export const convertTimeToStr = value => {
  const date = new Date(value);
  return (
    months[date.getMonth()] +
    " " +
    date.getDate() +
    ", " +
    date.getFullYear() +
    " " +
    convertSecondsToTimeString(date.getHours() * 3600 + date.getMinutes() * 60)
  );
};
