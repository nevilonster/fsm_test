import { css } from "styled-components";

const themeInputs = css`
  input[type="email"],
  input[type="password"],
  input[type="text"] {
    width: 286px;
    height: 32px;
    border-radius: 4px;
    font-size: 14px;
    line-height: 17px;
    font-family: "Poppins regular", serif;
  }
`;

export default themeInputs;
