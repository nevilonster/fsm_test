import styled from "styled-components";

export const ContentWrap = styled.div`
  margin: 1rem;
  display: flex;
  flex-direction: column;
  gap: 1rem;
`;

export const TitlePageWrap = styled.div``;
