import Router from "router";
import styled, { ThemeProvider } from "styled-components";
import { BrowserRouter } from "react-router-dom";

import theme from "globalStyles/theme";
import { routes } from "router/Configuration";
import ToastMessages from "components/toasts/ToastMessages";

function App() {
  return (
    <Wrap id={"appRoot"}>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Router routes={routes} />
        </BrowserRouter>
        <ToastMessages />
      </ThemeProvider>
    </Wrap>
  );
}

export default App;

const Wrap = styled.div`
  width: 100vw;
  height: 100vh;
`;
