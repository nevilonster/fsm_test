#### please use

```
"engines": {
    "npm": ">=9.6.2",
    "node": ">=v18.0.0"
},
```

### sources FSM

```src/libs/fsm```

### install dependencies:

```npm i```

### run tests:

```npm test```

### run project:

```npm run local```

## use 
```
const fsm = new FSM(alphabet, states, startState, finiteState, transitions);
fsm.exec(input);
```


